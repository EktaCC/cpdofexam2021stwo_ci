package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;
import com.agiletestingalliance.MinMax;

public class MinMaxTest {
    @Test
    public void testGetMax() throws Exception {
        final int resp = new MinMax().maxnum(1, 2);
        assertEquals("resp", 2, resp);
    }

    @Test
    public void testGetMax2() throws Exception {
        final int resp = new MinMax().maxnum(3, 2);
        assertEquals("resp", 3, resp);
    }

    @Test
    public void testBar2() throws Exception {
        final String resp = new MinMax().bar("hello");
        assertEquals("resp", "hello", resp);
    }              
}
